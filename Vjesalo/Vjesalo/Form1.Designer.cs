﻿namespace Vjesalo
{
    partial class FormVjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_word = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbox_guese = new System.Windows.Forms.TextBox();
            this.btn_guese = new System.Windows.Forms.Button();
            this.listBox_word = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lbl_tries = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tries: ";
            // 
            // lbl_word
            // 
            this.lbl_word.AutoSize = true;
            this.lbl_word.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_word.Location = new System.Drawing.Point(12, 124);
            this.lbl_word.Name = "lbl_word";
            this.lbl_word.Size = new System.Drawing.Size(67, 29);
            this.lbl_word.TabIndex = 1;
            this.lbl_word.Text = "Word";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Guese a letter:";
            // 
            // txtbox_guese
            // 
            this.txtbox_guese.Location = new System.Drawing.Point(136, 33);
            this.txtbox_guese.Name = "txtbox_guese";
            this.txtbox_guese.Size = new System.Drawing.Size(100, 20);
            this.txtbox_guese.TabIndex = 3;
            // 
            // btn_guese
            // 
            this.btn_guese.Location = new System.Drawing.Point(16, 78);
            this.btn_guese.Name = "btn_guese";
            this.btn_guese.Size = new System.Drawing.Size(75, 23);
            this.btn_guese.TabIndex = 4;
            this.btn_guese.Text = "Guese";
            this.btn_guese.UseVisualStyleBackColor = true;
            this.btn_guese.Click += new System.EventHandler(this.btn_guese_Click);
            // 
            // listBox_word
            // 
            this.listBox_word.FormattingEnabled = true;
            this.listBox_word.Location = new System.Drawing.Point(9, 210);
            this.listBox_word.Name = "listBox_word";
            this.listBox_word.Size = new System.Drawing.Size(189, 147);
            this.listBox_word.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(204, 210);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Select a word";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lbl_tries
            // 
            this.lbl_tries.AutoSize = true;
            this.lbl_tries.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tries.Location = new System.Drawing.Point(75, 161);
            this.lbl_tries.Name = "lbl_tries";
            this.lbl_tries.Size = new System.Drawing.Size(0, 22);
            this.lbl_tries.TabIndex = 8;
            // 
            // FormVjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 381);
            this.Controls.Add(this.lbl_tries);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox_word);
            this.Controls.Add(this.btn_guese);
            this.Controls.Add(this.txtbox_guese);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_word);
            this.Controls.Add(this.label1);
            this.Name = "FormVjesalo";
            this.Text = " ";
            this.Load += new System.EventHandler(this.FormVjesalo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_word;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbox_guese;
        private System.Windows.Forms.Button btn_guese;
        private System.Windows.Forms.ListBox listBox_word;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lbl_tries;
    }
}

